#![feature(negate_unsigned)]

extern crate docker;
extern crate getopts;
extern crate libc;
extern crate mkstemp;
extern crate tar;

mod gen;

use gen::{
    chroot,
    EvalContextNew,
    GenericAgentConfigApply,
    GenericAgentConfigNewDefault,
    GenericAgentConfigSetInputFile,
    GenericAgentDiscoverContext,
    GenericAgentPostLoadInit,
    GetInputDir,
    GetTTYInteractive,
    SelectAndLoadPolicy,
    AGENT_TYPE_AGENT,
    AGENT_CONTROL_ALWAYSVALIDATE
};

use docker::Docker;
use docker::image::Image;
use getopts::Options;
use std::env;
use std::ffi::CString;
use std::fs::File;
use std::io::{Result,Error,ErrorKind};
use std::os::unix::io::{FromRawFd,IntoRawFd};
use std::path::{PathBuf};
use std::process::{Command,exit,Stdio};
use tar::Archive;

fn usage(program: &str, opts: Options) -> ! {
    let brief = format!("Usage: {} [options] <image id>", program);
    println!("{}", opts.usage(&brief));
    exit(1);
}

fn check_image(uri: &str, name: &str) -> Result<Image> {
    let mut docker = match Docker::connect(uri) {
        Ok(docker) => docker,
        Err(e) => return Err(e)
    };

    let images = match docker.get_images(false) {
        Ok(images) => images,
        Err(e) => return Err(e)
    };

    for image in images {
        if image.Id.starts_with(name) {
            return Ok(image);
        }
    }

    Err(Error::new(ErrorKind::NotFound, format!("could not find image {}", name)))
}

fn extract_image(img: Image) -> Result<Box<PathBuf>> {
    let mut tmp = try!(mkstemp::tempfile("/tmp/docker.XXXXXX"));
    let out = try!(unsafe {
        Command::new("docker")
                .arg("save")
                .arg(img.Id)
                .stdout(Stdio::from_raw_fd(tmp.into_raw_fd()))
                .spawn()
    });

    tmp = unsafe { File::from_raw_fd(out.stdout.unwrap().into_raw_fd()) };
    let mut arc = Archive::new(tmp);
    let tmpdir = try!(mkstemp::tempdir("/tmp/docker.XXXXXX"));
    try!(arc.unpack(tmpdir.as_path()));

    Ok(tmpdir)
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optopt("f", "file", "Specify a CFEngine input file", "file");
    opts.optopt("D", "define", "Raise a CFEngine class", "class");
    opts.optopt("N", "negate", "Lower a CFEngine class", "class");
    opts.optopt("c", "connect", "Docker endpoint to connect to", "uri");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(_) => usage(&program, opts)
    };

    let image_name = if !matches.free.is_empty() {
        matches.free[0].clone()
    } else {
        usage(&program, opts)
    };

    let input_dir = unsafe { GetInputDir() };
    let agent = unsafe { GenericAgentConfigNewDefault(AGENT_TYPE_AGENT, GetTTYInteractive()) };

    if matches.opt_present("f") {
        let file = CString::new(matches.opt_str("f").unwrap()).unwrap();
        unsafe { GenericAgentConfigSetInputFile(agent, input_dir, file.into_raw()) };
    }

    let docker_uri = if matches.opt_present("c") {
        matches.opt_str("c").unwrap()
    } else {
        String::from("unix:///var/run/docker.sock")
    };

    // Check if the docker image that we're passed is legitimate
    let image = match check_image(&docker_uri, &image_name) {
        Ok(image) => image,
        Err(e) => {
            println!("{}", e);
            exit(2);
        }
    };

    // CFEngine init
    let ctx = unsafe { EvalContextNew() };
    let policy = unsafe {
        GenericAgentConfigApply(ctx, agent);
        GenericAgentDiscoverContext(ctx, agent);
        SelectAndLoadPolicy(agent, ctx, AGENT_CONTROL_ALWAYSVALIDATE as u8, true as u8)
    };

    if policy.is_null() {
        println!("Cannot load CFEngine policy");
        exit(3);
    }

    unsafe {
        GenericAgentPostLoadInit(ctx);
    }

    // Extract and chroot into Docker image
    let chroot_dir = match extract_image(image) {
        Ok(dir) => dir,
        Err(e) => {
            println!("error extracting: {}", e);
            exit(3);
        }
    };

    // WELP RUST HOPE THIS WORKS
    let dirstr = chroot_dir.as_path().to_str().unwrap();
    unsafe {
        chroot(CString::new(dirstr).unwrap().as_ptr());
    };
}
