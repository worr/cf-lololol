#![feature(convert)]
extern crate bindgen;
extern crate glob;

use glob::glob;
use std::fs;
use std::path::{Path,PathBuf};
use std::process::exit;
use std::io::{self,BufRead,Write,Error,ErrorKind};

fn main() {
    let gen_file = Path::new("src/gen.rs");
    let header = Path::new("gen_header.rs");
    let mut build = bindgen::builder();

    build.clang_arg("-I../cfengine/libpromises")
        .clang_arg("-I../cfengine/libutils")
        .clang_arg("-I../cfengine/libcfnet")
        .clang_arg("-I/usr/lib/clang/3.5.0/include")
        .clang_arg("-DHAVE_CONFIG_H")
        .emit_builtins()
        .header("headers.h");

    let res = gen(gen_file, header, build);
    if res.is_err() {
        println!("failed to write bindings: {}", res.err().unwrap());
        exit(1);
    }

    match find_lib("promises", &["/var/cfengine/lib"]) {
        Ok(dir) => {
            println!("cargo:rustc-link-search=native={}", dir.to_str().unwrap());
            println!("cargo:rustc-link-lib=promises")
        },
        Err(err) => {
            println!("{}", err);
            exit(2)
        }
    };
}

fn gen(filename: &Path, header: &Path, build: bindgen::Builder) -> Result<(), Error> {
    if filename.exists() {
        return Ok(())
    }

    let mut gen_out = Box::new(io::BufWriter::new(try!(fs::File::create(filename))));

    if header.exists() {
        let header_reader = io::BufReader::new(try!(fs::File::open(header)));
        for line in header_reader.lines() {
            try!(gen_out.write(line.unwrap().as_bytes()));
            try!(gen_out.write("\n".as_bytes()));
        }
    }

    match build.generate() {
        Ok(bindings) => bindings.write(gen_out),
        Err(()) => Err(Error::new(ErrorKind::Other, "Could not generate bindings"))
    }
}

fn find_lib(name: &str, other_locs: &[&str]) -> Result<Box<PathBuf>, Error> {
    let locations = vec![
        "/usr/lib",
        "/usr/lib64",
        "/lib",
        "/lib64",
        "/usr/local/lib",
        "/usr/local/lib64",
        "/opt/lib",
        "/usr/pkg/lib"
    ];

    for loc in locations {
        let lib_res = check_lib(name, loc);
        if lib_res.is_ok() {
            return Ok(lib_res.unwrap());
        }
    }

    for loc in other_locs {
        let lib_res = check_lib(name, loc);
        if lib_res.is_ok() {
            return Ok(lib_res.unwrap());
        }
    }

    Err(Error::new(ErrorKind::NotFound, name))
}

fn check_lib(name: &str, dir: &str) -> Result<Box<PathBuf>, Error> {
    let globin = dir.to_string() + "/lib" + name + ".so.?";
    let glob_res = glob(globin.as_str());
    if glob_res.is_err() {
        return Err(Error::new(ErrorKind::NotFound, name));
    }

    for file in glob_res.unwrap() {
        if file.is_ok() {
            let full = file.unwrap();
            let component = full.parent().unwrap();
            return Ok(Box::new(component.to_path_buf()));
        }
    }

    Err(Error::new(ErrorKind::NotFound, name))
}
