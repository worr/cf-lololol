#![allow(non_camel_case_types, non_snake_case, dead_code, non_upper_case_globals)]

pub type Enum_Unnamed163 = ::libc::c_uint;
pub const TIDY_LINK_DELETE: ::libc::c_uint = 0;
pub const TIDY_LINK_KEEP: ::libc::c_uint = 1;

pub type Enum_Unnamed221 = ::libc::c_uint;
pub const GENERIC_AGENT_CONFIG_COMMON_POLICY_OUTPUT_FORMAT_NONE: ::libc::c_uint = 0;
pub const GENERIC_AGENT_CONFIG_COMMON_POLICY_OUTPUT_FORMAT_CF: ::libc::c_uint = 1;
pub const GENERIC_AGENT_CONFIG_COMMON_POLICY_OUTPUT_FORMAT_JSON: ::libc::c_uint = 2;
